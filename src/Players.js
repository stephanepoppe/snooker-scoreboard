import React from 'react';

class Players extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)

        this.state = {
            playerOne: {value: '', error: null},
            playerTwo: {value: '', error: null}
        }
    }

    handleChange = (e) => {
        console.log(e.target.name);
        this.setState({[e.target.name]: {value: e.target.value}});
    }

    handleSubmit = (e) => {
        e.preventDefault();
        let valid = true;
        let playerOne = this.state.playerOne;
        let playerTwo = this.state.playerTwo;
        if (playerOne.value === '') {
            this.setState({playerOne: {value: '', error:"Selecteer een naam"}});
            valid = false;
        } else {
            playerOne.error = null;
            this.setState({playerOne: playerOne});
        }
        if (this.state.playerTwo.value === '') { 
            this.setState({playerTwo: {value: '', error:"Selecteer een naam"}});
            valid = false;
        } else {
            playerTwo.error = null;
            this.setState({playerTwo: playerTwo});
        }
        if (valid){
            this.props.onStart(this.state.playerOne.value, this.state.playerTwo.value);
        }        
    }

    render = () => { 
        return <div class="container">
            <section class="section">
                <div class="columns ">
                    <div class="column is-two-fifths">
                    <h1 class="title">Start spel</h1>
                    <form method="POST" onSubmit={this.handleSubmit}>
                        <div class="field">
                            <label class="label">Speler 1</label>
                            <div class="control">
                                <div class={this.state.playerOne.error !== null ? 'select is-danger' : 'select'}>
                                    <select name="playerOne" value={this.state.playerOne.value} onChange={this.handleChange}>
                                        <option>--Selecteer--</option>
                                        <option>Anke</option>
                                        <option>Antje</option>
                                        <option>Elke</option>
                                        <option>Geert</option>
                                        <option>Lena</option>
                                        <option>Stéphane</option>
                                    </select>
                                </div>
                            </div>
                            {this.state.playerOne.error !== null ? <p class="help is-danger">{this.state.playerOne.error}</p> : ''}
                        </div>

                        <div class="field">
                            <label class="label">Speler 2</label>
                            <div class="control">
                                <div class={this.state.playerTwo.error !== null ? 'select is-danger' : 'select'}>
                                    <select name="playerTwo" value={this.state.playerTwo.value} onChange={this.handleChange}>
                                        <option>--Selecteer--</option>
                                        <option>Anke</option>
                                        <option>Antje</option>
                                        <option>Elke</option>
                                        <option>Geert</option>
                                        <option>Lena</option>
                                        <option>Stéphane</option>
                                    </select>
                                </div>
                            </div>
                            {this.state.playerTwo.error !== null ? <p class="help is-danger">{this.state.playerOne.error}</p> : ''}
                        </div>

                        <div class="control">
                            <button class="button is-primary">Start</button>
                        </div>
                    </form>
                    </div>
                </div>  
            </section>
        </div>
    }
}

export default Players;