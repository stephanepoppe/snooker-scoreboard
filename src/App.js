import React from 'react';
import logo from './logo.svg';
import Header from './Header';
import Scores from './Scores';
import Players from './Players';

class App extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      gameStarted: false
    }
  }

  startGame = (playerOne, playerTwo) => {
    this.setState({
      gameStarted: true,
      playerOne: playerOne,
      playerTwo: playerTwo
    });
  }

  stopGame = () => {
    this.setState({
      gameStarted: false
    })
  }
 
  render() {
    const gameStarted = this.state.gameStarted;
    return (
      <div class="app">
        <Header />
        {gameStarted ? 
          <Scores playerOne={this.state.playerOne} playerTwo={this.state.playerTwo} onStopGame={this.stopGame} /> : 
          <Players onStart={this.startGame} /> }
      </div>)
    };
}

export default App;
