import React from 'react';

class Header extends React.Component {
    render(){
        return (<section class="hero is-primary">
        <div class="hero-body">
          <div class="container">
            <h1 class="title">
              Foxies scoreboard
            </h1>
          </div>
        </div>
      </section>)
    }
}

export default Header;