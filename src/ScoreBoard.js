import React from 'react';
import App from './App';

class ScoreBoard extends React.Component {
    constructor(props) {
      super(props);
      this.onAddScore = this.onAddScore.bind(this);
    }
  
    onAddScore(id, name, value, e) {
      e.preventDefault();
      this.props.onAddScore(id, name, value);
    }

    render() {
      const name = this.props.name;
      const id = this.props.id;
      return (<div class="tile is-vertical is-6 is-parent">
      <div class="tile is-child box">
          <p class="is-size-5 has-text-centered">{this.props.name}</p>
          <p class="is-size-1 has-text-centered has-text-weight-bold">{this.props.score}</p>
      </div>
      <div class="tile is-child box">
      <div class="tile is-child box">
        <article class="tile is-child notification is-light">
            <a onClick={(e) => this.onAddScore(id, name, 4, e)}  href="#"><i class="fas fa-circle" style={{color:"white", fontSize:"5em"}}></i></a>
            <a onClick={(e) => this.onAddScore(id, name, 1, e)}  href="#"><i class="fas fa-circle" style={{color:"red", fontSize:"5em"}}></i></a>
            <a onClick={(e) => this.onAddScore(id, name, 2, e)}  href="#"><i class="fas fa-circle" style={{color:"yellow", fontSize:"5em"}}></i></a>
            <a onClick={(e) => this.onAddScore(id, name, 3, e)}  href="#"><i class="fas fa-circle" style={{color:"green", fontSize:"5em"}}></i></a>
            <a onClick={(e) => this.onAddScore(id, name, 4, e)}  href="#"><i class="fas fa-circle" style={{color:"brown", fontSize:"5em"}}></i></a>
            <a onClick={(e) => this.onAddScore(id, name, 5, e)}  href="#"><i class="fas fa-circle" style={{color:"blue", fontSize:"5em"}}></i></a>
            <a onClick={(e) => this.onAddScore(id, name, 6, e)}  href="#"><i class="fas fa-circle" style={{color:"pink", fontSize:"5em"}}></i></a>
            <a onClick={(e) => this.onAddScore(id, name, 7, e)}  href="#"><i class="fas fa-circle" style={{color:"black", fontSize:"5em"}}></i></a>
        </article>
      </div>
      </div>
    </div>)
    } 
  }

  export default ScoreBoard;