import React from 'react';
import ScoreBoard from './ScoreBoard';
import Button from './Button';

class Scores extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            playerOne: { 
                score: 0,
                name: props.playerOne
            },
            playerTwo: { 
                score: 0,
                name: props.playerTwo
            },
            history: [],
        }
        this.onStop = this.onStop.bind(this); 
    }

    addScore = (id, name, score) => {    
        const playerState = this.state[id];
        const previousScore = playerState.score;
        playerState.score += score;
        this.setState({id: playerState});
        this.saveHistory(id, previousScore);
    }

    reset = (e) => {
        const playerOne = this.state.playerOne;
        playerOne.score = 0;
        const playerTwo = this.state.playerTwo;
        playerTwo.score = 0;
        this.setState({
            playerOne: playerOne,
            playerTwo: playerTwo
        });
        this.setState({history:[]});
    }

    undo = (e) => {
        const historyList = this.state.history;    
        const historyItem = historyList.shift();
        if (historyItem !== undefined) {
            const player = this.state[historyItem.id];
            player.score = historyItem.score;
            this.setState(player);
        }
    }

    onStop = (e) => {
        this.props.onStopGame();
    }

    saveHistory = (playerId, previousScore) => {
        const history = this.state.history;
        history.unshift(
            {
                id: playerId,
                score: previousScore
            });
        this.setState({history: history})
    }

    render = () => {
        const undoDisabled = this.state.history.length == 0;
        return <section class="section">
            <div class="container">
                <div class="tile is-ancestor">
                    <ScoreBoard id="playerOne" name={this.state.playerOne.name} score={this.state.playerOne.score} onAddScore={this.addScore}/>
                    <ScoreBoard id="playerTwo" name={this.state.playerTwo.name} score={this.state.playerTwo.score} onAddScore={this.addScore}/>
                    <div class="tile is-vertical is-parent">
                        <div class="tile is-child"></div>
                    </div>
                </div>
                <div class="buttons">
                    <Button onClick={this.undo} class="is-info" name="Undo" disabled={undoDisabled} />
                    <Button onClick={this.reset} class="is-danger" name="Reset scores" />
                    <Button onClick={this.onStop} class="is-danger" name="Stop" />
                </div>
            </div>
      </section>
    }
}

export default Scores;