import React from 'react';

class Button extends React.Component {
    constructor (props) {
        super(props);
        this.onClick = this.onClick.bind(this)
        this.state = {class: "button " + props.class};
    }

    onClick = () => {
        this.props.onClick();
    }

    render = () => {
        
        return (<a onClick={(e) => this.onClick(e)} disabled={this.props.disabled} class={this.state.class} >{this.props.name}</a>)
    } 
}

export default Button;